import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    jcenter()
    mavenCentral()
    maven(url = "http://dl.bintray.com/kotlin/ktor")
    maven(url = "http://dl.bintray.com/kotlin/kotlinx")
}

plugins {
    kotlin("jvm") version Versions.kotlinVersion
}

dependencies {

    implementation(kotlin("stdlib-jdk8"))

    // ktor
    fun ktor(suffix: String) = "io.ktor:ktor$suffix:${Versions.ktorVersion}"
    implementation(ktor(""))
    implementation(ktor("-gson"))
    implementation(ktor("-server-netty"))
    implementation(ktor("-client-core"))
    implementation(ktor("-client-apache"))

    // logging
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.microutils:kotlin-logging:1.6.26")

    // JWT
    implementation("io.jsonwebtoken:jjwt-api:0.10.7")
    runtime("io.jsonwebtoken:jjwt-impl:0.10.7")
    runtime("io.jsonwebtoken:jjwt-jackson:0.10.7")

    // REDIS
    implementation("redis.clients:jedis:2.10.2")
    testImplementation("com.github.fppt:jedis-mock:0.1.14")

    // misc
    implementation("com.google.cloud:google-cloud-datastore:1.72.0")

    // DEPENDENCY INJECTION
    // ----------------------
    implementation("com.google.inject:guice:4.2.2")
    implementation("org.kodein.di:kodein-di-generic-jvm:6.3.3")
    implementation("org.koin:koin-core:2.0.1")
    implementation("org.koin:koin-core-ext:2.0.1") //extended & experimental features
    testImplementation("org.koin:koin-test:2.0.1")

    // TEST
    // ============================================================

    // ktor integration tests
    testImplementation(ktor("-server-test-host"))
    testImplementation("org.mock-server:mockserver-netty:5.6.0")

    // FRAMEWORKS
    // ----------------------
    // JUNIT5
    testImplementation("org.junit.jupiter:junit-jupiter:5.5.1")
    // TESTNG
    testImplementation("org.testng:testng:6.14.3")
    // SPEK
    testImplementation("org.spekframework.spek2:spek-dsl-jvm:${Versions.spekVersion}")
    testRuntimeOnly("org.spekframework.spek2:spek-runner-junit5:${Versions.spekVersion}")
    testRuntimeOnly("org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlinVersion}") // spek requires kotlin-reflect, can be omitted if already in the classpath
    // KOTLIN TEST (more than a framework)
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")


    // MATCHER
    // ----------------------
    testImplementation("org.assertj:assertj-core:3.13.1")
//    testImplementation("com.willowtreeapps.assertk:assertk:0.19")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.19")
    testImplementation("org.hamcrest:hamcrest:2.1")
    testImplementation("com.natpryce:hamkrest:1.7.0.0")
    testImplementation("io.strikt:strikt-core:0.21.1")
    testImplementation("org.amshove.kluent:kluent:1.53")
    testImplementation("com.winterbe:expekt:0.2.0")
    testImplementation("ch.tutteli.atrium:atrium-cc-en_GB-robstoll:0.8.0")
    // MOCKING
    // ----------------------
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.1.0")
    testImplementation("io.mockk:mockk:1.9.3") // version suffix ".kotlin12" for old kotlin versions
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}
//tasks.withType<KotlinCompile>().configureEach {
//    kotlinOptions.jvmTarget = "1.8"
//}

//val test by tasks.getting(Test::class) {
//    useJUnitPlatform { }
//}
tasks.withType<Test> {
    useJUnitPlatform {
        //        includeEngines.add("spek2")
    }
}
//tasks.test {
//	useJUnitPlatform()
//	testLogging {
//		events("passed", "skipped", "failed")
//	}
//}
