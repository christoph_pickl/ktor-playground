object Versions {
    val kotlinVersion = "1.3.40"
    val ktorVersion = "1.1.5"
    val spekVersion = "2.0.5"
}
