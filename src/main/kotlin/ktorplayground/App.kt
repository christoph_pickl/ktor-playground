package ktorplayground

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.netty.EngineMain
import mu.KotlinLogging.logger

private val log = logger {}

fun main(args: Array<String>) {
    EngineMain.main(args)
}

// Referenced in application.conf
fun Application.startFoobar() {
    log.info { "Starting Ktor..." }
//    install(DefaultHeaders)
//    install(DefaultHeaders) {
//        header("X-Engine", "Ktor") // will send this header with each response
//    }
    routing {
        get("/") {
            call.respondText("Hello Douwe.")
        }
    }
}
