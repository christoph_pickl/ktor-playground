@file:Suppress("NonAsciiCharacters", "unused", "ClassName")

package ktorplayground

// from: https://williamdurand.fr/2013/06/03/object-calisthenics/

//<editor-fold desc="1. indentation">
// 1. Only One Level Of Indentation Per Method
// -==================================================-

// Having too many levels of indentation in your code is often bad for readability, and maintainability.
// Most of the time, you can’t easily understand the code without compiling it in your head,
// especially if you have various conditions at different level, or a loop in another loop, as shown in this example:

class `Board ❌` {

    fun board(): String {
        val buffer = StringBuilder()
        // 0 indent
        1.rangeTo(9).forEach { i ->
            // 1 indent
            1.rangeTo(9).forEach { j ->
                // 2 indent
                buffer.append("($i|$j)")
            }
            buffer.append("\n")
        }
        return buffer.toString()
    }
}

class `Board ✅` {
    fun board(): String {
        val buffer = StringBuilder()
        collectRows(buffer)
        return buffer.toString()
    }

    private fun collectRows(buffer: StringBuilder) {
        1.rangeTo(9).forEach { i ->
            collectRow(buffer, i)
        }
    }

    private fun collectRow(buffer: StringBuilder, row: Int) {
        1.rangeTo(9).forEach { i ->
            buffer.append("($row|$i)")
        }
        buffer.append("\n")
    }
}
//</editor-fold>

//<editor-fold desc="2. else">
// 2. Don’t Use The ELSE Keyword
// -==================================================-

// The else keyword is well-known as the if/else construct is built into nearly all programming languages.
// Do you remember the last time you saw a nested conditional? Did you enjoy reading it?
// I don’t think so, and that is exactly why it should be avoided.
// As it is so easy to add a new branch to the existing code than refactoring it to a better solution,
// you often end up with a really bad code.

// Instead use "early return" or also called "guardian clauses".
// Whereas that might interfer with the rule of not having more than x (1?) return statement in a method.

fun `save ❌`(username: String, data: Byte) {
    if (username != "isValid") {
        println("Log an error.")
    } else {
        // do some work ...
    }
}

fun `save ✅`(username: String, data: Byte) {
    if (username != "isValid") {
        println("Log an error.")
        return
    }
    // do some work ...
}

// alternatives:
// - introduce a variable to have parametrizable return statements (not always possible)
// - make use of polymorphism (State or Strategy pattern)
// - use the Null Object pattern

//</editor-fold>

//<editor-fold desc="3. wrap primitives">
// 3. Wrap All Primitives And Strings
// -==================================================-

//</editor-fold>

//<editor-fold desc="4. collections">
// 4. First Class Collections
// -==================================================-
//</editor-fold>

//<editor-fold desc="5. dot per line">
// 5. One Dot Per Line
// -==================================================-
//</editor-fold>

//<editor-fold desc="6. dont abbreviate">
// 6. Don’t Abbreviate
// -==================================================-
//</editor-fold>

//<editor-fold desc="7. small entities">
// 7. Keep All Entities Small
// -==================================================-
//</editor-fold>

//<editor-fold desc="8. instance vars">
// 8. No Classes With More Than Two Instance Variables
// -==================================================-
//</editor-fold>

//<editor-fold desc="9. no properties">
// 9. No Getters/Setters/Properties
// -==================================================-

//</editor-fold>