package ktorplayground.jwt

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import mu.KotlinLogging.logger
import java.util.concurrent.TimeUnit


private val log = logger {}

fun main() {
    startServer()
}

fun Application.startJwt() {

    install(ContentNegotiation) {
        gson {
        }
    }
    install(StatusPages) {
        installSecuredStatusPages()
    }
    routing {
        addUserRouting()
        addSecuredRouting()
    }
}


fun startServer() {
    log.info { "Starting up Netty server ..." }
    val server = embeddedServer(Netty, port = 8080) {
        startJwt()
    }

    server.start(wait = false)
    log.info { "Server started up." }

    println()
    println("--- Hit ENTER to exit ---")
    readLine()
    log.info { "Stopping server..." }
    server.stop(gracePeriod = 1L, timeout = 5L, timeUnit = TimeUnit.SECONDS)
    log.info { "Main ended." }
}
