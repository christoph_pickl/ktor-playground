@file:Suppress("EXPERIMENTAL_API_USAGE")

package ktorplayground.jwt

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.post
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

fun Routing.addUserRouting() {
    post("/login") {
        log.info { "/login requested" }
        // CARVE: doesnt work for POST: call.parameters["username"]
        val credentials = call.receive<Credentials>()
        if (UserService.login(credentials)) {
            val jwt = Jwter.encode {
                setSubject(credentials.username)
                // could set more data here ... setPayload()
            }
            JwtStore.register(jwt)
            call.respondText(text = jwt, contentType = ContentType.Text.Plain, status = HttpStatusCode.OK)
        } else {
            call.respondText(text = "Failed!", contentType = ContentType.Text.Plain, status = HttpStatusCode.Unauthorized)
        }
    }
}

object UserService {
    private val users = mapOf(
            "christoph" to "secret" // store encrypted password (bcrypt or tha-like)
    )

    fun login(credentials: Credentials): Boolean =
            users.any { it.key == credentials.username && it.value == credentials.password }
}

data class Credentials(
        val username: String,
        val password: String
)
