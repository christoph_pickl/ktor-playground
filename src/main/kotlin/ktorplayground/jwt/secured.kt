@file:Suppress("EXPERIMENTAL_API_USAGE")

package ktorplayground.jwt

import io.ktor.application.call
import io.ktor.features.BadRequestException
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import mu.KotlinLogging

private val log = KotlinLogging.logger {}
/*
    install(StatusPages) {
        exception<NotFoundException> { e ->
            call.respond(HttpStatusCode.NotFound, e)
        }
        exception<BadRequestException> { e ->
            call.respond(HttpStatusCode.BadRequest, e)
        }
        exception<Throwable> { e ->
            call.respond(HttpStatusCode.InternalServerError, e)
            throw e
        }
    }
 */
fun Routing.addSecuredRouting() {
    get("/secured") {
        log.info { "/secured requested" }

        // FIXME use a filter instead (along with configured endpoint scopes + configuration which endpoints require auth/which scope)
        val jwt = call.request.headers["token"] ?: throw BadRequestException("token required")
        if (!JwtStore.isRegistered(jwt)) {
            throw ForbiddenException()
        }
        call.respondText("OK")
    }
}

class ForbiddenException() : Exception()

fun StatusPages.Configuration.installSecuredStatusPages() {
    exception<ForbiddenException> { e ->
        call.respond(HttpStatusCode.Forbidden, e)
    }
}
