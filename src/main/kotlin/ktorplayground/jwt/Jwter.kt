package ktorplayground.jwt

import io.jsonwebtoken.*
import io.jsonwebtoken.security.Keys

fun main() {
    val key = Keys.secretKeyFor(SignatureAlgorithm.HS256)
    val jws = Jwts.builder().setSubject("mySubject").signWith(key).compact()
    println("Encoded JWS: [$jws]")

    val decoded = Jwts.parser().setSigningKey(key).parseClaimsJws(jws)
    println("Decoded JWS:")
    println("  body: ${decoded.body} (subject = ${decoded.body.subject})")
    println("  header: ${decoded.header}")
}

object Jwter {
    private val key = Keys.secretKeyFor(SignatureAlgorithm.HS256)

    fun encode(withBuilder: JwtBuilder.() -> Unit): String {
        return Jwts.builder().also {
            withBuilder(it)
        }.signWith(key).compact()
    }

    fun decode(encodedJwt: String): Jws<Claims> {
        try {
            return Jwts.parser().setSigningKey(key).parseClaimsJws(encodedJwt)
        } catch (e: JwtException) {
            throw SecurityException("Failed to decode JWT: [$encodedJwt]", e)
        }
    }

}

class SecurityException(message: String, cause: Exception) : Exception(message, cause)

object JwtStore {
    private val activeTokens = mutableSetOf<String>()

    fun register(jwt: String) {
        activeTokens += jwt
    }

    fun isRegistered(jwt: String) = activeTokens.contains(jwt)
}
