package investigations.testframework

import io.kotlintest.data.forall
import io.kotlintest.matchers.startWith
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row
import org.testng.annotations.Test
import kotlin.math.max

/*
https://github.com/kotlintest/kotlintest

CON: intellij integration not working properly (double click navigate to in report broken, execute single tests impossible)
CON: creates a nasty /.kotlintest/ directory
 */
@Test
class KotlinTestTest : StringSpec({
    "length should return size of string" {
        "hello".length shouldBe 5
    }

    "startsWith should test for a prefix" {
        "world" should startWith("wor")
    }

    "data driven test" {
        forall(
            row(1, 5, 5),
            row(1, 0, 1),
            row(0, 0, 0)
        ) { a, b, expectedMax ->
            max(a, b) shouldBe expectedMax
        }
    }

    val exception = shouldThrow<Exception> {
        throw Exception("error")
    }
    exception.message should startWith("er")
})
