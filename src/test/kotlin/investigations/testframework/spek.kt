package investigations.testframework

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

/*
ATTENTION: enable spek in gradle file: `includeEngines.add("spek2")`

CON: intellij runner integration doesnt work properly (double click navigate to in report broken)
 */
object SpekTest : Spek({

    class TestSubject {
        fun add(x: Int, y: Int) = x + y
    }

    describe("TestSubject methods") {
        val subject by memoized { TestSubject() }
        describe("executing add method") {
            it("returns the sum of its arguments") {
                assertEquals(3, subject.add(1, 2))
            }
        }
    }
})
