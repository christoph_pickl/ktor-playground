package investigations.testframework

import org.testng.annotations.*
import kotlin.test.assertTrue

/*
TestNG is the next generation test framework after JUnit
*/
// PRO: only you annotation on class level (not method level)
@Test(
    // PRO: can greate test groups (seperate different test levels, domains, technical (DB, redis), JIRA issues ...)
    groups = ["IntegrationTest", "KOT-42"]
)
class TestNGTest {
    init {
        // invoked once, reusing same instance for all test methods
        println("constructor()")
    }

    @BeforeClass fun beforeClass() {
        println("before class")
    }

    @BeforeMethod fun beforeMethod() {
        println("before method")
    }

    @AfterMethod fun afterMethod() {
        println("after method")
    }

    @AfterClass fun afterClass() {
        println("after class")
    }

    // PRO: no @Test needed
    fun `sample test`() {
        println("test")
    }

    @Test(
        expectedExceptions = [Exception::class],
        expectedExceptionsMessageRegExp = ".*testng.*",
        enabled = true // PRO: no extra annotation needed
    )
    fun `full test`() {
        throw Exception("hello testng!")
    }

    @Test(
        // only execute if the other is green
        dependsOnMethods = ["full test"] // runner creation fails if method not existing
    )
    fun `only when full test succeeds`() {
    }

    @DataProvider // PRO: sophisticated data providers
    fun provideNames() = arrayOf(
        arrayOf("Christoph"),
        arrayOf("Douwe"),
        arrayOf("Felippe")
    )

    @Test(dataProvider = "provideNames")
    fun testNames(name: String) {
        println("testing: $name")
        assertTrue(name[0].isUpperCase())
    }
}