package investigations.testframework

import org.junit.*

/*
JUnit is the most common test framework in the java ecosystem

PRO: supported by many many other tools out there
 */
class JUnitTest {
    init {
        // called for each test method, creating a fresh instance everytime
        println("constructor()")
    }

    companion object {

        @BeforeClass
        @JvmStatic // CON: has to be static for JUnit to work :-/
        fun beforeClass() {
            println("before class")
        }

        @AfterClass
        @JvmStatic
        fun afterClass() {
            println("after class")
        }
    }

    @Before fun beforeMethod() {
        println("before method")
    }

    @After fun afterMethod() {
        println("after method")
    }


    // CON: every test method has to have this annotation
    @Test(
        expected = Exception::class,
        // CON: no easy possibility for exception message testing (matcher library better anyway)
        timeout = 1_000L
    )
    fun `sample test 1`() {
        println("test 1")
        throw Exception()
    }

    @Test
    fun `sample test 2`() {
        println("test 2")
    }

    @Test
    @Ignore // CON: needs a second annotation, rather option in @Test
    fun `this is ignored alias disabled`() {
        println("test")
    }
}