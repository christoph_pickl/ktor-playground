package investigations.testframework

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.*
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

/*

JUnit 5 introduced @Theory, @Nested and much more

https://junit.org/junit5/docs/current/user-guide/

kupiter (kotlin DSL) is a one man show, actually just a single class adding tests dynamically (we could do this on our own too ;)
 */
@Tag("Integration")
@TestInstance(TestInstance.Lifecycle.PER_CLASS) // to avoid the need for static members
// can be defined globally by adding in `src/test/resources/junit-platform.properties` the content `junit.jupiter.testinstance.lifecycle.default = per_class`
class JUnit5Test {
    init {
        println("construct")
    }

    @BeforeAll
    fun beforeAll() {
        println("beforeAll")
    }

    @BeforeEach
    fun beforeEach() {
        println("beforeEach")
    }

    companion object {
        @JvmStatic // CON: static stuff as java-ish library
        fun intProvider() = listOf(0, 42, 9000)
    }

    @Test // has no params whatsoever...
    @Disabled
    fun foo() {
        // NOPE
//        val exception: Exception = assertThrows(IllegalArgumentException::class.java, {
//            throw IllegalArgumentException("exception message")
//        })
//        assertEquals("exception message", exception.message)

    }

    @TestFactory
    fun `Run multiple tests`(): Collection<DynamicTest> =
        listOf(2, 2, 3).map {
            dynamicTest("Check number $it is greater or equal than 2") {
                assertTrue(it >= 2)
            }
        }

    class Calculator {
        fun add(i: Int, j: Int) = i + j
        fun div(i: Int, j: Int) = i / j
    }

    @Test
    @Tag("Fast")
    fun `1 + 1 = 2`() {
        val calculator = Calculator()
        assertEquals(2, calculator.add(1, 1), "1 + 1 should equal 2")
    }

    @ParameterizedTest(name = "{0} + {1} => {2}")
    @CsvSource(
        "0,    1,   1",
        "1,    2,   3",
        "49,  51, 100",
        "1,  100, 101"
    )
    fun add(first: Int, second: Int, expectedResult: Int) {
        val calculator = Calculator()
        assertEquals(expectedResult, calculator.add(first, second)) {
            "$first + $second should equal $expectedResult"
        }
    }

    @Test
    fun divisionByZeroError() {
        val calculator = Calculator()
        val exception = assertThrows<ArithmeticException> {
            calculator.div(1, 0)
        }
        assertEquals("/ by zero", exception.message)
    }

    @ParameterizedTest
    @EnumSource(TimeUnit::class, mode = EnumSource.Mode.EXCLUDE, names = ["SECONDS", "MINUTES"])
    fun `Test enum without days and milliseconds`(timeUnit: TimeUnit) {
        println(timeUnit)
    }

    @ParameterizedTest
    @MethodSource("intProvider")
    fun `Test with custom arguments provider`(argument: Int) {
        println(argument)
        assertNotNull(argument)
    }

    // NOPE
    @Nested
    inner class `functionalities can be grouped this way` {
        @Test
        fun subFunc() {
            println("subFunc")
        }

        @ParameterizedTest
        @ArgumentsSource(PersonProvider::class)
        fun `Check age greater or equals 18`(person: Person) {
            assertTrue(person.name.length > 1)
        }
    }

}

class PersonProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> = Stream.of(
        Person("John"),
        Person("Jane"),
        Person("Ivan")
    ).map { Arguments.of(it) }
}

class Person(val name: String)