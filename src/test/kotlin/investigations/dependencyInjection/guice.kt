package investigations.dependencyInjection


import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Guice.createInjector
import com.google.inject.util.Modules

/*
there are (at least 2) nice Kotlin API wrappers available.

PRO: from master google (will last, other than the minor OSS projects)
PRO: assisted inject FTW

see: https://github.com/google/guice
*/
object GuiceDemo {

    fun foo() {
        val injector = Guice.createInjector(BusinessModule(), UiModule())
        val billingService = injector.getInstance(Controller::class.java)
    }
}

class AppModule : AbstractModule() {
    override fun configure() {
        install(UiModule())
        install(BusinessModule())
    }
}


class UiModule : AbstractModule() {
    override fun configure() {
        // ??? or install here business module? (explicit dependencies! ok with installing multiple times?!)
        bind(Controller::class.java)
    }
}

class BusinessModule : AbstractModule() {
    override fun configure() {
        bind(Service::class.java).to(ServiceImpl::class.java)
    }
}

object GuiceTest {
    class TestModule : AbstractModule() {
        override fun configure() {
            // re-define here the Service bean
            bind(Service::class.java).to(TestableService::class.java)
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        `here we are explicitly overriding beans`()
    }

    fun `here we are explicitly overriding beans`() {
        val injector = createInjector(Modules.override(AppModule()).with(TestModule()))
        injector.getInstance(Controller::class.java).say()
    }
}

// KOTLIN API over GUICE
// https://github.com/misfitlabsdev/kotlin-guice
/*
class MyModule : KotlinModule() {
    override fun configure() {
        bind<Service>().to<ServiceImpl>().`in`<Singleton>()
        bind<PaymentService<CreditCard>>().to<CreditCardPaymentService>()
        bind<CreditCardProcessor>().annotatedWith<PayPal>().to<PayPalCreditCardProcessor>()
    }
}
fun main(args: Array<String>) {
  val injector = Guice.createInjector(MyModule(), MyPrivateModule())
  val paymentService = injector.getInstance<PaymentService<CreditCard>>()
  val payPalProcessor = injector.getInstance(annotatedKey<CreditCardProcessor, PayPayl>())
}
 */


// YET another project (only 2 contributers, looks a bit unloved)
// https://github.com/JLLeitschuh/kotlin-guiced
/*
fun main(vararg args: String) {
    val myModule = module {
        bind<SomeService>().to<SomeServiceImpl>()
    }
    val injector = Guice.createInjector(myModule)
}
 */
