package investigations.dependencyInjection

import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

/*

PRO: ktor support

https://github.com/InsertKoinIO/koin
 */

val appModule = module {
    single<Service> { ServiceImpl() }
    single { Controller(get()) }
}

object KoinDemo {
    @JvmStatic
    fun main(args: Array<String>) {
        println("koin")

        val koin = startKoin {
            modules(appModule)
        }
        koin.koin.get<Controller>().say()
    }
}

class MyKoinTest : KoinTest {
    private val controller: Controller by inject()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            startKoin {
                modules(listOf(appModule, module {
                    // this does the OVERRIDE trick (otherwise fails with an exception +1)
                    single<Service>(override = true) { TestableService() }
                }))
            }
//            koin.koin.get<Controller>().say()
            MyKoinTest().someTestMethod()
        }
    }

    fun someTestMethod() {
        println("from TEST:")
        controller.say()
        // same as: get<Controller>().say()
    }
}
