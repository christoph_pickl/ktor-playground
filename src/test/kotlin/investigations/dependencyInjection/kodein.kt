package investigations.dependencyInjection


import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

/*
Supercessor of Injekt.

PRO: integration with Ktor: https://kodein.org/Kodein-DI/?6.3/ktor

https://kodein.org/di/
https://kodein.org/Kodein-DI/?6.3/getting-started
https://kodein.org/Kodein-DI/?6.3/core
*/


val uiModule = Kodein.Module(name = "UI Module") {
    println("building UI module")
//    bind() from singleton { Controller(instance()) }
    bind<Controller>() with provider { Controller(instance()) }
    // bind<Dice>() with provider { RandomDice(0, 5) }

//    bind<Database>(tag = "local") with singleton { SQLiteDatabase() }
//    bind<Database>(tag = "remote") with provider { DatabaseProxy() }
}

val businessModule = Kodein.Module("Business Module") {
    println("building business module")
    bind<Service>() with provider { ServiceImpl() }
}

val appKodein = Kodein {
    import(businessModule)
    import(uiModule)
}


object KodeinDemo {
    @JvmStatic
    fun main(args: Array<String>) {
        `full sample with modules for better seperation`()
    }

    fun `full sample with modules for better seperation`() {
//        val controller by kodein.newInstance { Controller(instance()) }
        val controller by appKodein.instance<Controller>()
        controller.say()

        val presenter = Presenter(appKodein)
        presenter.say()
    }

}

// very tight coupling to kodein, but handy (testability?!)
class Presenter(override val kodein: Kodein) : KodeinAware {

    private val service: Service by instance()
    fun say() {
        println("Presenter says: ${service.serviceMessage()}")
    }
}

object KodeinTest {
    @JvmStatic
    fun main(args: Array<String>) {
        foo()
    }

    fun foo() {
        val testKodein = Kodein {
            println("extend app kodein")
            extend(appKodein)
            // HERE happens the magic
            // PRO: override bean by mistake throws an exception
            println("override service")
            // this doesnt work when Controller is defined as singleton in UI module :-(
            bind<Service>(overrides = true) with provider { TestableService() }
        }
        println("get instance()")
        val controller by testKodein.instance<Controller>()
        println("say:")
        controller.say()
    }
}
