package investigations.dependencyInjection


import javax.inject.Inject

/*

dependencies {
    compile(kotlin("stdlib"))
    implementation("com.google.inject:guice:4.2.2")

    // Use kodein-generic-jvm or kodein-erased-jvm.

    implementation("org.kodein.di:kodein-di-generic-jvm:6.3.3")

    implementation("org.koin:koin-core:2.0.1")
    implementation("org.koin:koin-core-ext:2.0.1") // extended & experimental features
    testImplementation("org.koin:koin-test:2.0.1")
}

 */
// CON: guice needs @Inject annotation on constructor (or zero arg ctor)
class Controller @Inject constructor(
    val service: Service
) {

    init {
        println("new Controller(service=$service)")
    }

    fun say() {
        println("Controller says: ${service.serviceMessage()}")
    }
}

interface Service {
    fun serviceMessage(): String
}

class ServiceImpl() : Service {
    init {
        println("new ServiceImpl()")
    }

    override fun serviceMessage() = "production system"
}

class TestableService : Service {
    override fun serviceMessage() = "from test"
}
