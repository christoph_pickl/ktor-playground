package investigations.mocking

import com.nhaarman.mockitokotlin2.*
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.testng.annotations.Test


/*

https://github.com/nhaarman/mockito-kotlin
 */
@Test
class MockitoTest {
    interface TestInterface {
        fun say(): String
        fun doSomething(i: Int)
        fun forbidden()
    }

    class Person(val name: String)

    fun foo() {
        val mock: TestInterface = mock()
        whenever(mock.say()).thenReturn("hello")
//        val mock = mock<TestInterface> {
//            on { say() } doReturn "hello"
//        }
//        whenever(mock.forbidden()).doThrow(RuntimeException("do throw"))
//        whenever(mock.forbidden()).thenThrow(RuntimeException::class.java)
//        whenever(mock.forbidden()).thenThrow(RuntimeException("then throw"))


        println(mock.say())
        println(mock.say())
        mock.doSomething(1)

        verify(mock, times(2)).say() // by default times=1
        verify(mock).doSomething(any())
        verifyNoMoreInteractions(mock)
//        verifyOrder {}

//        argumentCaptor<String>().apply {
//            verify(myClass, times(2)).setItems(capture())
//
//            assertEquals(2, allValues.size)
//            assertEquals("test", firstValue)
//        }

        // SPY
        class Spyable {
            fun getMeaning(input: String) = "42"
        }

        val spyable = Spyable()
        val spy = Mockito.spy<Any>(spyable) // CON: to mock kotlin (final) classes, need mockito-extensions configuration
        `when`(spyable.getMeaning(anyString()))
            .thenThrow(RuntimeException::class.java)
        spyable.getMeaning("any")

        val mockProperties = mock<Person>()
        whenever(mockProperties.name).thenReturn("a")
    }
}