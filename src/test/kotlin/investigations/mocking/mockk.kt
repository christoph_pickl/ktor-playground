package investigations.mocking

import io.mockk.every
import io.mockk.mockk
import org.testng.annotations.Test
import kotlin.test.assertEquals
import kotlin.test.fail

/*
Mockk allows for a more functional mocking style compared to Mockito.

PRO: can mock final classes by default

see: https://github.com/mockk/mockk
 */
@Test
class MockkTest {
    private interface TestInterface {
        fun name(): String
        fun number(): Int
        fun exception(): String
    }

    fun mockedCase() {
        // mock interface
        val mockedInterface = mockk<TestInterface>()

        // define actions with every function
        every { mockedInterface.name() }.returns("test")
        every { mockedInterface.number() }.returnsMany(listOf(1, 2, 3))
        every { mockedInterface.exception() }.throws(Exception("test"))

        // assert the created mock
        assertEquals("test", mockedInterface.name())
        assertEquals(1, mockedInterface.number())
        assertEquals(2, mockedInterface.number())
        assertEquals(3, mockedInterface.number())

        try {
            mockedInterface.exception()
            fail()
        } catch (e: Exception) {
            assertEquals("test", e.message)
        }
    }
}