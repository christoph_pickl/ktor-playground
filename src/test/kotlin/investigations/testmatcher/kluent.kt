package investigations.testmatcher

import io.kotlintest.matchers.equality.shouldBeEqualToIgnoringFields
import io.kotlintest.matchers.string.shouldContainIgnoringCase
import org.amshove.kluent.*
import org.testng.annotations.Test

/*
Veeeery fluent Kotlin-ish :)
(infix, backticks, extensions on primitive types)

Kluent wraps mockito-kotlin to provide a fluent API.
Kluent also wraps JUnit-Asserts

see: https://markusamshove.github.io/Kluent/
 */
@Test
class KluentTest {
    interface TestInterface {
        fun say(): String
    }

    fun foo() {
        "hello" shouldEqual "hello"
        "hello" `should equal` "hello" // ==
        "hello" `should not equal` "world" // !=
        val identity = "identity"
        identity `should be` identity // ===

        42 // PRO: chaining is possible
            .`should be positive`()
            .`should be greater than`(21)

        val maybe: String? = "Hello"
        maybe.shouldNotBeNull()
            .`should not be empty`()
            .shouldContainIgnoringCase("hel")

        listOf(1, 2) shouldContain 2

        val stub = mock(TestInterface::class)
        When calling stub.say() itReturns "mocked value"

        data class Person(val id: Int, val name: String)

        val person = Person(1, "Christoph")
        person.shouldBeEqualToIgnoringFields(Person(2, "Christoph"), Person::id)
    }

    interface Database {
        fun getPerson(): Person
        fun getPerson(id: Int): Person
    }

    data class Person(val name: String, val surname: String)


    fun mocking() {
// Verify that a method was called
        var mock = mock(Database::class)
        mock.getPerson(5)
        Verify on mock that mock.getPerson() was called

// Verify that a method was not called
        mock = mock(Database::class)
        mock.getPerson(1)
        VerifyNotCalled on mock that mock.getPerson(5)

// Verify that a method with any parameter was called
        mock = mock(Database::class)
        mock.getPerson(200)
        Verify on mock that mock.getPerson(any()) was called

// Verify no interactions
        mock = mock(Database::class)
        VerifyNoInteractions on mock

// Verify no further interactions
        mock = mock(Database::class)
        mock.getPerson(1)
        mock.getPerson(5)
        Verify on mock that mock.getPerson(1) was called
        Verify on mock that mock.getPerson(5) was called
        VerifyNoFurtherInteractions on mock
    }

    fun stubbing() {
        val bob = Person("Bob", "Guy")
        val alice = Person("Alice", "Person")

// Let a stub return an instance
        var stub = mock(Database::class)
        When calling stub.getPerson() itReturns bob
        stub = mock(Database::class)
        When calling stub.getPerson(any()) itReturns alice

// Let a stub throw an exception
        stub = mock(Database::class)
        When calling stub.getPerson() itThrows RuntimeException("An exception")
    }

}

infix fun <T> T.`should be equal to`(expected: T?): T = shouldEqual(expected)