package investigations.testmatcher

import ch.tutteli.atrium.api.cc.en_GB.and
import ch.tutteli.atrium.api.cc.en_GB.isGreaterThan
import ch.tutteli.atrium.api.cc.en_GB.isLessThan
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import org.testng.annotations.Test

// https://www.kotlinresources.com/library/atrium/
@Test
class AtriumTest {
    fun foo() {
        val x = 10
        expect(x).toBe(10)

        expect(4 + 6).isLessThan(5).isGreaterThan(10)

        expect(4 + 6) {
            isLessThan(5)
            isGreaterThan(10)
        }

        expect(4 + 6) {
            // ...
        } and {
            // ...
        }
    }
}