package investigations.testmatcher

import assertk.Assert
import assertk.all
import assertk.assertAll
import assertk.assertThat
import assertk.assertions.*
import org.testng.annotations.Test

/*
assertions for KOTLIN inspired by assertj

see: https://github.com/willowtreeapps/assertk
 */
@Test
class AssertKTest {
    fun foo() {
        assertThat(2).isEqualTo(2)

        val nullString: String? = "null"
        assertThat(nullString).isNotNull().hasLength(4) // PRO: aware of kotlin's null type-safety

        assertThat("Hello ")
            // CON: chaining not possible, have to use all{}
            .all {
                startsWith("H")
                contains("he", ignoreCase = true)
                hasLineCount(1)
            }

        assertThat {
            throw Exception("error")
        }.isFailure()
            .all {
                hasMessage("error")
                stackTrace().doesNotContain("secret")
            }

        data class Person(val name: String, val age: Int)

        val person = Person("World", 30)
        assertThat(person.age, "age").isGreaterThan(20)
        assertThat(person::name).isEqualTo("World") // -> expected [name]:<["Alice"]> but was:<["Bob"]>

        assertAll {
            // ensure all assertions will run, even if first one fails
            assertThat(false).isTrue()
            assertThat(true).isFalse()
        }

        // customization:
        fun Assert<Person>.hasAge(expected: Int) {
            prop("age", Person::age).isEqualTo(expected)
        }
        assertThat(person).hasAge(12) // PRO: easy for example for `assertThat(response).hasStatusOk()`
    }
}