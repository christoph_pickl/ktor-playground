package investigations.testmatcher

import org.testng.annotations.Test
import strikt.api.Assertion
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.*
import java.time.LocalDate
import java.time.MonthDay

/*

https://strikt.io/
 */
@Test
class StriktTest {


    fun `expectThat simple`() {

        val subject = "The Enlightened take things Lightly"
        expectThat(subject)
            .hasLength(35)
            .matches(Regex("[\\w\\s]+"))
            .startsWith("T")

        expectThat(3) {
            isGreaterThan(1)
            isEqualTo(3)
            isLessThan(4)
        }
//      ▼ Expect that 3:
//        ✓ is greater than 1
//        ✓ is equal to 3
//        ✗ is less than 2

        expectThat(listOf(10, 2, 3))
            .contains(10, 2, 3)
            .any {
                isGreaterThan(5) // connected with logical AND, as "any" refers to any single element (not assertions)
                isEven()
            }

        expect {
            that(1).isEqualTo(1)
            that(2).isEqualTo(2)
        }

        val person = Person("Christoph", 33)
        expectThat(person)
            .get(Person::name)
            .startsWith("Chr")

        // CUSTOMIZATION
        fun Assertion.Builder<LocalDate>.isStTibsDay() =
            assert("is St. Tib's Day") {
                when (MonthDay.from(it)) {
                    MonthDay.of(2, 29) -> pass()
                    else -> fail()
                }
            }
        expectThat(LocalDate.of(2020, 2, 29)).isStTibsDay()

        expectThat(person)
            .description
            .isEqualTo("Christoph is 33")

    }
}

private data class Person(val name: String, val age: Int)

private val Assertion.Builder<Person>.description: Assertion.Builder<String>
    get() = get { "$name is $age" }

fun Assertion.Builder<Int>.isEven(): Assertion.Builder<Int> =
    assert("is even") {
        when {
            (it % 2) == 0 -> pass()
            else -> fail()
        }
    }