package investigations.testmatcher

import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat
import org.testng.annotations.Test

/*
Hamcrest for Kotlin

CON: bad documentation

https://github.com/npryce/hamkrest
 */
@Test
class HamkrestTest {
    fun foo() {
        assertThat(2, equalTo(2))

        assertThat("xyzzy", startsWith("x") and endsWith("y") and !(containsSubstring("a") or containsSubstring("b")))
        assertThat("xyzzy", allOf(startsWith("x"), endsWith("y"), containsSubstring("zz")))
        assertThat("xyzzy", has(String::length, equalTo(5)))

        val nullable: String? = "xyzzy"
        assertThat(nullable, present(startsWith("x")))

        assertThat(listOf(1, 2, 3), allOf(
            hasSize(equalTo(5)),
            hasSize(5), // doesn't exist by default :-/
            hasElement(2)
        ))

        val isBlank = Matcher(String::isBlank)
        assertThat("", isBlank)

        fun String.twoRestMatches(rest: Int): Boolean = this.length % 2 == rest
        val isOfEvenLength = Matcher(String::twoRestMatches, 0)
        assertThat("1234", isOfEvenLength)

        val isLongEnough = has(String::length, greaterThan(8))
        assertThat("password1", isLongEnough)
    }
}

fun hasSize(expected: Int) = has(Collection<Any>::size, equalTo(expected))
