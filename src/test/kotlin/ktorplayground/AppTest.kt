package ktorplayground

import io.ktor.application.Application
import io.ktor.http.HttpMethod.Companion.Get
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test

@Test
class AppTest {

    fun `GET url`() {
        withTestApplication(Application::startFoobar) {
            with(handleRequest(Get, "/")) {
                // TODO how about a custom assertion here ;)
                assertThat(response.status()).isEqualTo(OK)
                assertThat(response.content).isEqualTo("Hello Douwe.")
            }
        }
    }

}
