package ktorplayground

import com.google.cloud.datastore.*
import com.google.cloud.datastore.testing.LocalDatastoreHelper
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import java.util.*

@Test
class GoogleDatastoreTest {

    private val consistency = 1.0
    private lateinit var server: LocalDatastoreHelper
    private lateinit var client: Datastore
    private val projectId = "xxx"
    private val kind = "kind"

    @BeforeClass
    fun `start server`() {
        server = LocalDatastoreHelper.create(consistency)
        server.start()

        client = DatastoreOptions.newBuilder()
                .setHost("http://localhost:${server.port}")
                .setProjectId(projectId)
                .build()
                .service
    }

    @AfterClass
    fun `stop server`() {
        server.stop()
    }

    @BeforeMethod
    fun `reset data`() {
        server.reset()
    }

    fun `do some test`() {
        client.runInTransaction {
            it.put(FullEntity.newBuilder()
                    .setKey(IncompleteKey.newBuilder(projectId, kind).build())
                    .set("name", "Christoph")
                    .build())
        }

        val query = Query.newEntityQueryBuilder()
//                .setKind(INDEX)
//                .setFilter(StructuredQuery.PropertyFilter.eq("id", id.value.toString()))
//                .setOrderBy(StructuredQuery.OrderBy.asc("version"))
                .build()

        val result: QueryResults<Entity> = client.run(query)
//        client.get(Key.Builder())

        println("More results: ${result.moreResults}")
        result.forEach {
            println("Found entity: $it")
        }
        val resultList = result.toList()
        assertThat(resultList).hasSize(1)
        assertThat(resultList[0].getString("name")).isEqualTo("Christoph")
    }
}

fun <T> Iterator<T>.toList(): LinkedList<T> {
    val list = LinkedList<T>()
    while (hasNext()) {
        list.add(next())
    }
    return list
}
