package ktorplayground.jwt

import io.ktor.application.Application
import io.ktor.http.HttpMethod.Companion.Get
import io.ktor.http.HttpMethod.Companion.Post
import io.ktor.http.HttpStatusCode.Companion.Forbidden
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.http.HttpStatusCode.Companion.Unauthorized
import io.ktor.server.testing.*
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test


@Test
class JwtAppTest {

    private val validUsername = "christoph"
    private val validPassword = "secret"
    private val invalidPassword = "invalid"

    fun `login successfully`() = withJwtApp {
        val response = executePost("/login") {
            addJsonContentHeader()
            setBody(constructLoginBody(validUsername, validPassword))
        }

        assertThat(response.status()).isEqualTo(OK)
        assertThat(response.content).isNotEmpty()
    }

    fun `login failed as of wrong password`() = withJwtApp {
        val response = executePost("/login") {
            addJsonContentHeader()
            setBody(constructLoginBody(validUsername, invalidPassword))
        }

        assertThat(response.status()).isEqualTo(Unauthorized)
    }

    fun `request secured with valid token`() = withJwtApp {
        val jwt = loginAs("christoph", "secret")
        val response = executeGet("/secured") {
            addHeader("token", jwt)
        }

        assertThat(response.status()).isEqualTo(OK)
    }

    fun `request secured with invalid token`() = withJwtApp {
        val response = executeGet("/secured") {
            addHeader("token", "invalidToken")
        }

        assertThat(response.status()).isEqualTo(Forbidden)
    }

    private fun TestApplicationRequest.addJsonContentHeader() {
        addHeader("Content-Type", "application/json")
    }

    private fun <R> withJwtApp(test: TestApplicationEngine.() -> R) {
        withTestApplication(Application::startJwt, test)
    }

    private fun TestApplicationEngine.loginAs(username: String, password: String): String {
        val response = executePost("/login") {
            addJsonContentHeader()
            setBody(constructLoginBody(username, password))
        }
        assertThat(response.status()).isEqualTo(OK)
        assertThat(response.content).isNotEmpty().isNotNull()
        return response.content!! // safe due to isNotNull() above
    }

    private fun constructLoginBody(username: String, password: String) =
            """{ "username": "$username", "password": "$password" }"""


    private fun TestApplicationEngine.executePost(uri: String, setup: TestApplicationRequest.() -> Unit = {}) =
            handleRequest(Post, uri, setup).response

    private fun TestApplicationEngine.executeGet(uri: String, setup: TestApplicationRequest.() -> Unit = {}) =
            handleRequest(Get, uri, setup).response

}
