package ktorplayground

import com.github.fppt.jedismock.RedisServer
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import redis.clients.jedis.Jedis

@Test
class JedisTest {


    private lateinit var client: Jedis
    private lateinit var server: RedisServer
    private val someValue = "someValue"

    @BeforeClass
    fun `start server`() {
        server = RedisServer.newRedisServer()
        server.start()
        client = Jedis(server.host, server.bindPort)
    }

    @AfterClass
    fun `stop server`() {
        server.stop()
    }

    @BeforeMethod
    fun `reset data`() {
        client.flushAll()
    }

    fun `Given some value stored When get it Then it is returned`() {
        client.set("key", someValue)

        val received = client.get("key")

        assertThat(received).isEqualTo(someValue)
    }

    fun `When get unknown key Then it is empty`() {
        val received = client.get("key")

        assertThat(received).isNull()
    }

}